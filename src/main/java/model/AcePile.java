package model;

import java.util.ArrayList;

import org.joml.Rectangled;

public class AcePile extends Pile {

	private int pileNum; // number assigned to pile
	private CardSuit suit; // suit assigned to pile

	/**
	 * constructor
	 * 
	 * @param pileNum
	 */
	public AcePile(int pileNum) {
		super(new ArrayList<Card>(), "ace", new Rectangled(10 + pileNum * 3, 12.0, 12 + pileNum * 3, 15.0));
		this.pileNum = pileNum;
		suit = null;
	}

	@Override
	public boolean addCard(Card card) {
		if (checkAddingCard(card)) {
			Card next = getNext();
			if (next != null) {
				if (CardDenomination.previous(getNext()) != CardDenomination.ordinal(card)) {
					return false;
				}
			} else {
				if (CardDenomination.ordinal(card) != 0) {
					return false;
				}
				suit = card.getSuit();
			}
			cards.add(card);
			changeCardPile(card);
			return true;
		}
		return false;
	}

	/**
	 * returns the pile's suit
	 * 
	 * @return
	 */
	public CardSuit getSuit() {
		return suit;
	}

	/**
	 * sets the pile's suit
	 * 
	 * @param suit
	 */
	public void setSuit(CardSuit suit) {
		if (this.suit == null) {
			this.suit = suit;
		}
	}

}
