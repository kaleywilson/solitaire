package graphics;

import java.util.Iterator;

import org.joml.Rectangled;

import com.jogamp.opengl.GL2;

import lib342.Colors;
import lib342.opengl.Constants.PrimitiveType;
import model.Card;
import model.Pile;
import mygraphicslib.GLUtilities;

public class TempDrawer implements IPileDrawer {
	private IDrawCard cardDrawer;

	@Override
	public void drawPile(GL2 gl, Pile pile) {
		Highlight highlight = new Highlight(pile.getHighlight());
		double gap = 0.3 * 3; // default gap

		// check and find gap
		if (9 < pile.size() * gap + 3) {
			gap = 6 / (double) pile.size();
		}

		// draw each card in the pile
		int i = 0;
		Iterator<Card> it = pile.iterator();
		while (it.hasNext()) {
			Card card = it.next();
			if (card.isVisible()) {
				cardDrawer = new VisibleCard();
			} else {
				cardDrawer = new InvisibleCard();
			}
			Rectangled rect = pile.getRectangle();
			cardDrawer.drawCard(gl, card, highlight,
					new Rectangled(rect.minX, rect.minY - gap * i, rect.maxX, rect.maxY - gap * i));
			
			i++;
		}

		// if pile is empty then draw outline
		if (pile.isEmpty()) {
			Rectangled rect = pile.getRectangle();
			double cx = (rect.maxX - rect.minX) / 2 + rect.minX;
			double cy = (rect.maxY - rect.minY) / 2 + rect.minY;
			double width = (rect.maxX - rect.minX);
			double height = rect.maxY - rect.minY;
			highlight.getHighlight(gl, false);
			gl.glBegin(PrimitiveType.LINE_LOOP.getValue());
			{
				GLUtilities.drawRoundedRectangle(gl, cx, cy, width, height, .1);
			}
			gl.glEnd();
			gl.glColor3fv(Colors.BLACK, 0);
			gl.glLineWidth(1.0f);
		}
	}

}
