package model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.joml.Rectangled;

/**
 * This interface is used to create piles
 * 
 * @author kaley
 *
 */
public abstract class Pile {
	protected List<Card> cards; // list of cards
	protected String name; // name of pile
	protected Rectangled rectangle; // the position of pile based on world coordinates
	protected boolean highlight; // highlight

	/**
	 * constructor
	 * 
	 * @param cards     list of cards to be added in pile
	 * @param name      name of pile
	 * @param rectangle position of pile
	 */
	public Pile(List<Card> cards, String name, Rectangled rectangle) {
		this.cards = cards;
		this.name = name;
		highlight = false;
		this.rectangle = rectangle;
		changeCardsPile(cards);
	}

	/**
	 * setter for highlight boolean
	 * 
	 * @param b true/false
	 */
	public void setHighlight(boolean b) {
		highlight = b;
	}

	/**
	 * returns highlight for pile
	 * 
	 * @return highlight
	 */
	public boolean getHighlight() {
		return highlight;
	}

	/**
	 * returns the next card that can be used/seen
	 * 
	 * @return
	 */
	public Card getNext() {
		try {
			return cards.get(cards.size() - 1);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	/**
	 * gets the card in this pile based off the denomination
	 * 
	 * @param i represents value of denomination
	 * @return card that matches the conditions
	 */
	public Card getCardByDenom(CardDenomination denom) {
		for (Card card : cards) {
			if (card.isVisible()) {
				if (denom == card.getDenomination()) {
					return card;
				}
			}
		}
		return null;
	}

	/**
	 * primarily used for temp pile. used to get a list of cards below the card
	 * passed in
	 * 
	 * @param card
	 * @return
	 */
	public List<Card> getCards(Card card) {
		List<Card> cardsR = new ArrayList<Card>();
		try {
			int i = cards.indexOf(card);
			for (int j = i; j < cards.size(); j++) {
				cardsR.add(cards.get(j));
			}
			return cardsR;
		} catch (NullPointerException e) {
			return null;
		}
	}

	/**
	 * changes a list of cards names of the pile it is in
	 * 
	 * @param cards
	 */
	public void changeCardsPile(List<Card> cards) {
		for (Card card : cards) {
			changeCardPile(card);
		}
	}

	/**
	 * changes a card's name of the pile it now is in
	 * 
	 * @param card
	 */
	public void changeCardPile(Card card) {
		card.changePile(CardPile.getCardPile(name));
	}

	/**
	 * checks the card against the pile
	 * 
	 * @param card
	 * @return
	 */
	public boolean checkAddingCard(Card card) {
		return CardPile.addCard(this, card.getPile());
	}

	/**
	 * removes card from the list of cards
	 * 
	 * @param card Card to be removed
	 * @return boolean
	 */
	public boolean removeCard(Card card) {
		try {
			return cards.remove(card);
		} catch (NullPointerException e) {
			return false;
		}
	}

	/**
	 * adds card to a pile if the conditions are met for each different pile
	 * 
	 * @param card
	 * @return boolean
	 */
	public abstract boolean addCard(Card card);

	/**
	 * reAdding a card to the original sourcePile if an action is cancelled
	 * 
	 * @param card Card being added
	 * @return boolean
	 */
	public boolean reAddCard(Card card) {
		return cards.add(card);
	}

	/**
	 * moves card from the pile if the conditions are met for each different pile
	 * 
	 * @param card
	 * @return boolean
	 */
	protected boolean moveCard(Pile pile, Card card) {
		if (pile.addCard(card)) {
			return this.removeCard(card);
		} else {
			return false;
		}
	}

	/**
	 * returns size of the cards list
	 * 
	 * @return int
	 */
	public int size() {
		return cards.size();
	}

	/**
	 * returns a boolean if it is empty or not
	 * 
	 * @return boolean
	 */
	public boolean isEmpty() {
		return cards.isEmpty();
	}

	/**
	 * gets the first card in the pile
	 * 
	 * @return
	 */
	public Card getFirst() {
		return cards.get(0);
	}

	/**
	 * an iterator that starts from the end of the list of the pile
	 * 
	 * @return
	 */
	public ListIterator<Card> rIterator() {
		return cards.listIterator(size());
	}

	/**
	 * iterator that starts from the start of the list of the pile
	 * 
	 * @return
	 */
	public Iterator<Card> iterator() {
		return cards.iterator();
	}

	/**
	 * to string method
	 */
	public String toString() {
		String s = "\n";
		for (int i = size() - 1; i >= 0; i--) {
			s = s.concat(cards.get(i).toString() + " ");
		}
		return s;
	}

	/**
	 * returns a retangled that is associated to the pile's position in world
	 * coordinates
	 * 
	 * @return
	 */
	public Rectangled getRectangle() {
		return rectangle;
	}
}
