package model;

import java.util.ArrayList;
import java.util.List;

/**
 * this class is abstract and allows a deck of cards
 * to be constructed and helps set up the card table
 * @author kaley
 *
 */
public abstract class Deck {
	protected List<Card> cards;

	/**
	 * constructor
	 */
	public Deck() {
		cards = new ArrayList<Card>();
		createCards();
	}

	/**
	 * creates all the cards of the deck
	 */
	public void createCards() {
		for (CardSuit suit : CardSuit.values()) {
			for (CardDenomination denom : CardDenomination.values()) {
				cards.add(new Card(suit, denom));
			}
		}
	}

	/**
	 * shuffles the card depending on the class extending Deck
	 */
	public abstract void shuffle();

	/**
	 * returns the list of cards
	 * 
	 * @return cards
	 */
	public List<Card> getCards() {
		return cards;
	}

	/**
	 * returns the next card and removes it from the deck
	 * 
	 * @return	Card
	 */
	public Card nextCard() {
		try {
			return cards.remove(cards.size() - 1);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}
}
