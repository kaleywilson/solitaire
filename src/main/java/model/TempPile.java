package model;

import java.util.List;

import org.joml.Rectangled;

public class TempPile extends Pile {
	/**
	 * constructor
	 * 
	 * @param cards
	 * @param numPile
	 */
	public TempPile(List<Card> cards, int numPile) {
		super(cards, "temp", new Rectangled(1.0 + 3 * numPile, 7.0, 3.0 + 3 * numPile, 10.0));
	}

	@Override
	public boolean addCard(Card card) {
		if (canAdd(card)) {
			cards.add(card);
			changeCardPile(card);
			return true;
		}
		return false;
	}

	/**
	 * checks to see if a card can be added to the temp pile
	 * 
	 * @param card
	 * @return
	 */
	public boolean canAdd(Card card) {
		if (checkAddingCard(card)) {
			Card next = getNext();
			if (card.isVisible()) {
				if (next != null) {
					if (CardDenomination.next(getNext()) != CardDenomination.ordinal(card)) {
						return false;
					} else if (CardSuit.checkSuit(getNext(), card)) {
						return false;
					} else if(!next.isVisible()) {
						return false;
					}
				} else {
					if (CardDenomination.ordinal(card) != 12) {
						return false;
					}
				}
			} else {
				if (next != null && next.isVisible()) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean moveCard(Pile pile, Card card) {
		int index = cards.indexOf(card);
		boolean move = false;
		try {
			Card current = cards.get(index);
			while (pile.addCard(current)) {
				this.removeCard(current);
				move = true;
				current = cards.get(index);
			}
		} catch (IndexOutOfBoundsException e) {
		}
		return move;
	}

	@Override
	public boolean removeCard(Card card) {
		try {
			int i = cards.indexOf(card);
			try {
				Card prevCard = cards.get(i - 1);
				if (!prevCard.isVisible()) {
					prevCard.setVisible(true);
				}
			} catch (IndexOutOfBoundsException e) {

			}
			return cards.remove(card);
		} catch (NullPointerException e) {
			return false;
		}
	}

	/**
	 * just removes the card from the pile, but doesn't reveal the card underneath
	 * 
	 * @param card
	 * @return
	 */
	public boolean eraseCard(Card card) {
		return cards.remove(card);
	}

}
