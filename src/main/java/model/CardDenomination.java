package model;

import org.joml.Rectangled;

import com.jogamp.opengl.GL2;

import lib342.opengl.Constants.PrimitiveType;
import mygraphicslib.GLUtilities;

public enum CardDenomination {
	ace(0), two(1), three(2), four(3), five(4), six(5), seven(6), eight(7), nine(8), ten(9), jack(10), queen(11),
	king(12);

	private int value;

	private CardDenomination(int value) {
		this.value = value;
	}

	/**
	 * returns the card denomination based on the value passed int
	 * 
	 * @param value
	 * @return
	 */
	public static CardDenomination getDenom(int value) {
		for (CardDenomination denom : CardDenomination.values()) {
			if (denom.value() == value) {
				return denom;
			}
		}
		return null;
	}

	/**
	 * returns value of the denomination
	 * 
	 * @return
	 */
	public int value() {
		return this.value;
	}

	/**
	 * returns the int that should be before it
	 * 
	 * @param card is the card already on the pile
	 * @return int value of card
	 */
	public static int previous(Card card) {
		return ordinal(card) + 1;
	}

	/**
	 * returns int of value that should be next
	 * 
	 * @param card
	 * @return
	 */
	public static int next(Card card) {
		return ordinal(card) - 1;
	}

	/**
	 * returns the value of the denomination of a card
	 * 
	 * @param card
	 * @return
	 */
	public static int ordinal(Card card) {
		CardDenomination denom = card.getDenomination();
		return denom.value();
	}

	/**
	 * draws the denomination according to the rect
	 * 
	 * @param gl
	 * @param denom
	 * @param rect
	 */
	public static void drawDenomination(GL2 gl, CardDenomination denom, Rectangled rect) {
		gl.glLineWidth(2.0f);
		switch (denom) {
		case ace:

			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			gl.glVertex2d(rect.minX + 0.1, rect.maxY - 0.80);
			gl.glVertex2d(rect.minX + 0.3, rect.maxY - 0.20);
			gl.glVertex2d(rect.minX + 0.5, rect.maxY - 0.80);
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			double slope = 0.6 / 0.2;
			gl.glVertex2d(rect.minX + 0.2, rect.maxY - 0.40);
			gl.glVertex2d(rect.minX + 0.4, rect.maxY - 0.40);

		}
			gl.glEnd();
			break;
		case two:

			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + .30, rect.maxY - 0.4, .15, 140, -180);

			gl.glVertex2d(rect.minX + 0.15, rect.maxY - 0.80);
			gl.glVertex2d(rect.minX + 0.45, rect.maxY - 0.80);
		}
			gl.glEnd();
			break;
		case three:
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + .25, rect.maxY - 0.35, .15, 140, -180);
			GLUtilities.drawArc(gl, rect.minX + .25, rect.maxY - 0.65, .15, 90, -240);
		}
			gl.glEnd();
			break;
		case four:
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			gl.glVertex2d(rect.minX + 0.4, rect.maxY - 0.80);
			gl.glVertex2d(rect.minX + 0.4, rect.maxY - 0.20);
			gl.glVertex2d(rect.minX + 0.15, rect.maxY - 0.50);
			gl.glVertex2d(rect.minX + 0.45, rect.maxY - 0.50);
		}
			gl.glEnd();
			break;
		case five:
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			gl.glVertex2d(rect.minX + 0.45, rect.maxY - 0.20);
			gl.glVertex2d(rect.minX + 0.10, rect.maxY - 0.20);
			gl.glVertex2d(rect.minX + 0.10, rect.maxY - 0.60);
			GLUtilities.drawArc(gl, rect.minX + 0.30, rect.maxY - 0.60, .22, 135, -280);
		}
			gl.glEnd();
			break;
		case six:
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + 0.30, rect.maxY - 0.6, .2, -180, 360);
			GLUtilities.drawArc(gl, rect.minX + 0.30, rect.maxY - 0.4, .2, -180, -165);

		}
			gl.glEnd();
			break;
		case seven:
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			gl.glVertex2d(rect.minX + 0.1, rect.maxY - 0.25);
			gl.glVertex2d(rect.minX + 0.4, rect.maxY - 0.25);
			gl.glVertex2d(rect.minX + 0.20, rect.maxY - 0.85);
		}
			gl.glEnd();
			break;
		case eight:
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + 0.3, rect.maxY - 0.40, .15, -90, 360);
			GLUtilities.drawArc(gl, rect.minX + 0.3, rect.maxY - 0.70, .15, 90, 360);
		}
			gl.glEnd();
			break;
		case nine:
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + 0.30, rect.maxY - 0.4, .2, 0, 360);
			GLUtilities.drawArc(gl, rect.minX + 0.30, rect.maxY - 0.6, .2, 0, -165);
		}
			gl.glEnd();
			break;
		case ten:
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			gl.glVertex2d(rect.minX + 0.12, rect.maxY - 0.23);
			gl.glVertex2d(rect.minX + 0.12, rect.maxY - 0.80);
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.LINE_LOOP.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + 0.35, rect.maxY - 0.40, .17, 15, 150);
			GLUtilities.drawArc(gl, rect.minX + 0.35, rect.maxY - 0.63, .17, 195, 150);
		}
			gl.glEnd();
			break;
		case jack:
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			gl.glVertex2d(rect.minX + 0.4, rect.maxY - 0.2);
			GLUtilities.drawArc(gl, rect.minX + 0.25, rect.maxY - 0.65, 0.15, 0, -165);
		}
			gl.glEnd();
			break;
		case queen:
			gl.glBegin(PrimitiveType.LINE_LOOP.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + 0.3, rect.maxY - 0.3, .17, 15, 150);
			GLUtilities.drawArc(gl, rect.minX + 0.3, rect.maxY - 0.6, .17, 195, 150);

		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			gl.glVertex2d(rect.minX + 0.33, rect.maxY - 0.64);
			gl.glVertex2d(rect.minX + 0.50, rect.maxY - 0.77);
		}
			gl.glEnd();
			break;
		case king:
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			gl.glVertex2d(rect.minX + 0.15, rect.maxY - 0.2);
			gl.glVertex2d(rect.minX + 0.15, rect.maxY - 0.8);
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.LINE_STRIP.getValue()); {
			gl.glVertex2d(rect.minX + .43, rect.maxY - 0.2);
			gl.glVertex2d(rect.minX + 0.15, rect.maxY - 0.55);
			gl.glVertex2d(rect.minX + 0.43, rect.maxY - 0.8);

		}
			gl.glEnd();
			break;
		}
		gl.glLineWidth(1.0f);
	}

	@Override
	public String toString() {
		switch (this) {
		case ace:
			return "A";
		case two:
			return "2";
		case three:
			return "3";
		case four:
			return "4";
		case five:
			return "5";
		case six:
			return "6";
		case seven:
			return "7";
		case eight:
			return "8";
		case nine:
			return "9";
		case ten:
			return "10";
		case jack:
			return "J";
		case queen:
			return "Q";
		case king:
			return "K";
		default:
			return "";
		}
	}

}
