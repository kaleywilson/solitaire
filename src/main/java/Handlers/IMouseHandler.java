package Handlers;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import solitaire.EventHandler;

public abstract class IMouseHandler extends MouseAdapter {
	protected EventHandler eventHandler;

	public IMouseHandler(EventHandler handler) {
		eventHandler = handler;
	}

	@Override
	public abstract void mouseClicked(MouseEvent e);

	@Override
	public abstract void mouseDragged(MouseEvent e);

}
