package Handlers;

import java.awt.event.KeyEvent;

import solitaire.Command;
import solitaire.EventHandler;

public class SelectTempKeyHandler extends IKeyHandler {

	/**
	 * constructor
	 * 
	 * @param handler
	 */
	public SelectTempKeyHandler(EventHandler handler) {
		super(handler);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		switch (e.getKeyChar()) {
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case 'e':
		case '*':
			Command.doCommand(e.getKeyChar(), eventHandler, this);
			break;
		default:
			break;
		}
	}

}
