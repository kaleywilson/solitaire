package graphics;

import java.io.IOException;
import java.io.InputStream;

import org.joml.Vector2d;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import lib342.Colors;
import lib342.GraphicsWindowFP;
import lib342.opengl.Utilities;
import lib342.opengl.Constants.MatrixMode;
import lib342.opengl.Constants.PrimitiveType;
import mygraphicslib.McFallGLUtilities;

public class TextureFixedPipeline extends GraphicsWindowFP {

	Texture texture;
	
	
	public TextureFixedPipeline() {
		super("Playing with FP textures", 600, 600);
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		try {
			InputStream is = getClass().getResourceAsStream("RyanMcFall2.jpg");						
			texture = TextureIO.newTexture(is, false, "jpg");
		} catch (GLException | IOException e) {
			System.err.println("Exception loading texture: " + e);
		}
		
		GL2 gl = (GL2) canvas.getGL();
		gl.glPointSize(10);
	}
	
	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = (GL2) canvas.getGL();
		McFallGLUtilities.clearColorBuffer(gl, Colors.BLACK);
		McFallGLUtilities.setWorldWindow(gl, -2, 2, -2, 2);
		gl.glViewport(0, 0, Utilities.getCanvasWidth(canvas), Utilities.getCanvasHeight(canvas));
		
		
		texture.enable(gl);
		texture.bind(gl);
		
		gl.glBegin(PrimitiveType.POLYGON.getValue());
		{
			//  Texture coordinates range from (0,1) in both the horizontal and vertical directions, so this 
			//  associates the bottom left corner of the image with the bottom left corner of the rectangle
			//  It's important to issue texture coordinates before vertices.
			gl.glTexCoord2d(0, 0);
			gl.glVertex2d(-2, 0);
			
			//  This one is the bottom right corner of the texture
			gl.glTexCoord2d(1, 0);
			gl.glVertex2d(0, 0);
			
			//  This one is the top right corner of the texture
			gl.glTexCoord2d(1, 1);
			gl.glVertex2d(0, 2);
			
			//  And the top left corner
			gl.glTexCoord2d(0, 1);
			gl.glVertex2d(-2, 2);									
		}
		gl.glEnd();
		
		//  We're now going to draw triangle with vertices computed from a circle 
		//  centered at (1, -1) (middle of the bottom right quadrant of our window) with a radius of 1 .  
		//  The vertices will be at -30 degrees, 90 degrees, and 210 degrees
		//  For our texture, we'll center the circle at (0.5, 0.5) which is the center of the texture.
		//  We'll use a radius of 0.5 and pick the same angles as above.
		gl.glColor3fv(Colors.WHITE, 0);
		double textureRadius = 0.5;
		double startAngle = -30;
		
		gl.glBegin(PrimitiveType.TRIANGLE_FAN.getValue());; 
		{		
			for (int i = 0; i < 3; i++) {
				double angle = startAngle + i * 120;
				Vector2d txtCoords = McFallGLUtilities.pointOnCircle(0.5, 0.5, textureRadius, angle);				
				gl.glTexCoord2d(txtCoords.x, txtCoords.y);
				
				Vector2d pt = McFallGLUtilities.pointOnCircle(1, -1, 0.75, angle);
				gl.glVertex2d(pt.x, pt.y);			
			}
		}
		gl.glEnd();
		
		//  Now we'll show where the texture cooordinates are located on the original image
		//  Use (0,1), (0,1) as our world window since that matches texture space
		McFallGLUtilities.setWorldWindow(gl, 0, 1, 0, 1);
		
		//  And set the viewport to the upper left half of our screen window.
		gl.glViewport(0, Utilities.getCanvasHeight(canvas)/2, Utilities.getCanvasWidth(canvas)/2, Utilities.getCanvasHeight(canvas)/2);
		
		
		texture.disable(gl);
		gl.glBegin(PrimitiveType.POINTS.getValue());
		{			
			//  The location of the center used to calculate texture coordinates
			gl.glVertex2d(0.5, 0.5);
			
			for (int i = 0; i < 3; i++) {				
				Vector2d txtCoords = McFallGLUtilities.pointOnCircle(0.5, 0.5, textureRadius, startAngle + i*120);				
				gl.glVertex2d(txtCoords.x, txtCoords.y);
			}
		}		
		gl.glEnd();
		
		gl.glBegin(PrimitiveType.LINE_LOOP.getValue());
		{
			for (int i = 0; i < 3; i++) {				
				Vector2d txtCoords = McFallGLUtilities.pointOnCircle(0.5, 0.5, textureRadius, startAngle + i*120);				
				gl.glVertex2d(txtCoords.x, txtCoords.y);
			}
		}
		gl.glEnd();
	}

	public static void main(String[] args) {
		new TextureFixedPipeline().setVisible(true);

	}

}