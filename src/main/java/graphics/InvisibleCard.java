package graphics;

import org.joml.Rectangled;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;

import lib342.Colors;
import lib342.opengl.Constants.PrimitiveType;
import model.Card;
import mygraphicslib.GLUtilities;
import solitaire.Solitaire;

public class InvisibleCard implements IDrawCard {

	@Override
	public void drawCard(GL2 gl, Card card, Highlight highlight, Rectangled rect) {
		double cx = (rect.maxX - rect.minX) / 2 + rect.minX;
		double cy = (rect.maxY - rect.minY) / 2 + rect.minY;
		double width = (rect.maxX - rect.minX);
		double height = rect.maxY - rect.minY;

		Texture texture = Solitaire.texture;

		// draw textured card
		gl.glColor3fv(Colors.WHITE, 0);
		texture.enable(gl);
		texture.bind(gl);
		gl.glBegin(PrimitiveType.POLYGON.getValue());
		{
			GLUtilities.drawTextureRoundedRectangle(gl, cx, cy, width, height, .05);
		}
		gl.glEnd();

		texture.disable(gl);

		// highlight
		highlight.getHighlight(gl, false);
		gl.glBegin(PrimitiveType.LINE_LOOP.getValue());
		{
			GLUtilities.drawRoundedRectangle(gl, cx, cy, width, height, .05);
		}
		gl.glEnd();
		gl.glColor3fv(Colors.BLACK, 0);
		gl.glLineWidth(1.0f);
	}

}
