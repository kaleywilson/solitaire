package model;

public class Card {
	private CardSuit suit;
	private CardDenomination denomination;
	private CardPile pileType; // types of Pile (ace, draw, draw, temp, view)
	private boolean visible;

	/**
	 * constructor
	 * 
	 * @param suit
	 * @param denomination
	 */
	public Card(CardSuit suit, CardDenomination denomination) {
		this.suit = suit;
		this.denomination = denomination;
		visible = false;
		pileType = CardPile.temp;

	}

	/**
	 * returns the type of pile
	 * 
	 * @return CardPile
	 */
	public CardPile getPile() {
		return pileType;
	}

	/**
	 * chante the CardPile type
	 * 
	 * @param pileType
	 */
	public void changePile(CardPile pileType) {
		this.pileType = pileType;
	}

	/**
	 * returns suit of card
	 * 
	 * @return suit
	 */
	public CardSuit getSuit() {
		return suit;
	}

	/**
	 * returns denomination of card
	 * 
	 * @return denom
	 */
	public CardDenomination getDenomination() {
		return denomination;
	}

	/**
	 * returns if it visilb
	 * 
	 * @return boolean
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * sets its visibility
	 * 
	 * @param visible
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	@Override
	public String toString() {
		return denomination.toString() + suit.toString();
	}
}
