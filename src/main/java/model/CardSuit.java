package model;

import org.joml.Rectangled;

import com.jogamp.opengl.GL2;

import lib342.opengl.Constants.PrimitiveType;
import mygraphicslib.GLUtilities;

/**
 * this class creates a card based on the suit and provides methods to get
 * details based on the value and suit how to shuffle? create each card
 * individually? or shuffle in another class and get the details in here
 * 
 * @author kaley
 *
 */
public enum CardSuit {
	Spade, Heart, Diamond, Club;

	public String toString() {
		switch (this) {
		case Spade:
			return "S";
		case Heart:
			return "H";
		case Diamond:
			return "D";
		case Club:
			return "C";
		default:
			return "";
		}
	}

	/**
	 * returns a boolean is the two cards can be places on eachother
	 * 
	 * @param card1
	 * @param card2
	 * @return
	 */
	public static boolean checkSuit(Card card1, Card card2) {
		switch (card1.getSuit()) {
		case Spade:
		case Club:
			return (card2.getSuit() == CardSuit.Club || card2.getSuit() == CardSuit.Spade);
		case Heart:
		case Diamond:
			return (card2.getSuit() == CardSuit.Diamond || card2.getSuit() == CardSuit.Heart);
		}
		return false;
	}

	/**
	 * draws the suit based on the rectangled passed in
	 * 
	 * @param gl
	 * @param suit
	 * @param rect
	 */
	public static void drawSuit(GL2 gl, CardSuit suit, Rectangled rect) {

		switch (suit) {
		case Diamond:
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			gl.glVertex2d(rect.minX + 0.80, rect.maxY - 0.85);
			gl.glVertex2d(rect.minX + 1.05, rect.maxY - 0.50);
			gl.glVertex2d(rect.minX + 0.80, rect.maxY - 0.15);
			gl.glVertex2d(rect.minX + 0.55, rect.maxY - 0.50);
		}
			gl.glEnd();
			break;
		case Club:
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			GLUtilities.drawRoundedRectangle(gl, rect.minX + 0.80, rect.maxY - .60, .15, .3, 0);
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + .65, rect.maxY - 0.50, .15, 0, 360);
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + .80, rect.maxY - 0.33, .15, 0, 360);
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + .95, rect.maxY - 0.50, .15, 0, 360);
		}
			gl.glEnd();
			break;

		case Heart:
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			gl.glVertex2d(rect.minX + 0.80, rect.maxY - .80);
			GLUtilities.glVertex2dv(gl, GLUtilities.pointOnCircle(rect.minX + .90, rect.maxY - 0.40, 0.17, -35));
			GLUtilities.glVertex2dv(gl, GLUtilities.pointOnCircle(rect.minX + .70, rect.maxY - 0.40, 0.17, 215));
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + 0.70, rect.maxY - 0.40, 0.17, 0, 360);
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + .90, rect.maxY - 0.40, 0.17, 0, 360);
		}
			gl.glEnd();
			break;

		case Spade:
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			gl.glVertex2d(rect.minX + 0.80, rect.maxY - 0.15);
			GLUtilities.glVertex2dv(gl, GLUtilities.pointOnCircle(rect.minX + .90, rect.maxY - 0.50, 0.17, 35));
			GLUtilities.glVertex2dv(gl, GLUtilities.pointOnCircle(rect.minX + .70, rect.maxY - 0.50, 0.17, 145));
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + 0.70, rect.maxY - 0.50, 0.17, 0, 360);
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			GLUtilities.drawArc(gl, rect.minX + 0.90, rect.maxY - 0.50, 0.17, 0, 360);
		}
			gl.glEnd();
			gl.glBegin(PrimitiveType.POLYGON.getValue()); {
			GLUtilities.drawRoundedRectangle(gl, rect.minX + .80, rect.maxY - 0.60, .2, .4, 0);
		}
			gl.glEnd();
			break;
		}
	}
}
