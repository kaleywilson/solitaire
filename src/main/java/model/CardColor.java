package model;

import com.jogamp.opengl.GL2;

import lib342.Colors;

public enum CardColor {
	Red, Black;

	/**
	 * sets color in openGL
	 * @param gl
	 * @param suit
	 */
	public static void getColor(GL2 gl, CardSuit suit) {
		switch (suit) {
		case Diamond:
		case Heart:
			gl.glColor3fv(Colors.RED, 0);
			break;
		case Spade:
		case Club:
			gl.glColor3fv(Colors.BLACK, 0);
			break;
		}
	}

}
