package model;

public enum CardPile {
	draw, ace, temp, view, drag;

	/**
	 * checks which piles, a pile can exchange card with
	 * 
	 * @param pile
	 * @param pileType
	 * @return
	 */
	public static boolean addCard(Pile pile, CardPile pileType) {
		String sPile = pileType.toString();
		if (pile instanceof ViewPile) {
			return (sPile.equals("draw"));
		} else if (pile instanceof TempPile) {
			if (sPile.equals("ace") || sPile.equals("view") || sPile.equals("temp") || sPile.equals("drag")) {
				return true;
			}
		} else if (pile instanceof AcePile) {
			if (sPile.equals("view") || sPile.equals("temp") || sPile.equals("drag")) {
				return true;
			}
		} else if (pile instanceof DragPile) {
			return true;
		}
		return false;
	}

	/**
	 * gets the CardPile with a string
	 * 
	 * @param sPile
	 * @return
	 */
	public static CardPile getCardPile(String sPile) {
		if (sPile.equals("view")) {
			return CardPile.view;
		} else if (sPile.equals("draw")) {
			return CardPile.draw;
		} else if (sPile.equals("ace")) {
			return CardPile.ace;
		} else if (sPile.equals("temp")) {
			return CardPile.temp;
		} else if (sPile.equals("drag")) {
			return CardPile.drag;
		}
		return null;
	}

}