package solitaire;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import Handlers.IKeyHandler;
import solitaire.Command;

public class Reader {

	/**
	 * reads a file and does the actions written on it
	 * 
	 * @param reader
	 * @param eventHandler
	 * @param kHandler
	 */
	public static void readFile(FileReader reader, EventHandler eventHandler, IKeyHandler kHandler) {
		BufferedReader buffer = new BufferedReader(reader);
		String line;
		try {
			while ((line = buffer.readLine()) != null) {
				line = line.trim();
				String[] command = new String[0];
				if (!line.startsWith("#")) {
					command = line.split("\\s+");
				}
				for (String com : command) {
					com.trim();
					String s;
					int repeat;

					int i = 0;
					if (com.length() > 1) {
						while (Character.isDigit(com.charAt(i))) {
							i++;
						}
						if (i > 0) {
							repeat = Integer.parseInt(com.substring(0, i));
						} else {
							repeat = 1;
						}
						s = com.substring(i);
					} else {
						repeat = 1;
						s = com;
					}

					for (int j = 0; j < repeat; j++) {
						for (int k = 0; k < s.length(); k++) {
							kHandler = eventHandler.updateHandler();
							Command.doCommand(s.charAt(k), eventHandler, kHandler);
						}
					}

				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
