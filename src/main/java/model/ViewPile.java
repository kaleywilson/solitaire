package model;

import java.util.List;

import org.joml.Rectangled;

public class ViewPile extends Pile {

	/**
	 * constructor
	 * passes in a predetermined rectangle
	 * @param cards
	 */
	public ViewPile(List<Card> cards) {
		super(cards, "view", new Rectangled(4.0, 12.0, 6.0, 15.0));
	}

	@Override
	public boolean addCard(Card card) {
		if (checkAddingCard(card)) {
			card.setVisible(true);
			cards.add(card);
			changeCardPile(card);
			return true;
		}
		return false;
	}

}
