package graphics;

import org.joml.Rectangled;

import com.jogamp.opengl.GL2;

import lib342.Colors;
import lib342.opengl.Constants.PrimitiveType;
import model.Card;
import model.Pile;
import mygraphicslib.GLUtilities;

public class DefaultPileDrawer implements IPileDrawer {
	private IDrawCard cardDrawer;

	/**
	 * constructor
	 */
	public DefaultPileDrawer() {
	}

	@Override
	public void drawPile(GL2 gl, Pile pile) {
		Highlight highlight = new Highlight(pile.getHighlight());
		try {
			Card card = pile.getNext();
			if (card.isVisible()) {
				cardDrawer = new VisibleCard();
			} else {
				cardDrawer = new InvisibleCard();
			}
			cardDrawer.drawCard(gl, card, highlight, pile.getRectangle());

		} catch (NullPointerException e) {
			Rectangled rect = pile.getRectangle();
			double cx = (rect.maxX - rect.minX) / 2 + rect.minX;
			double cy = (rect.maxY - rect.minY) / 2 + rect.minY;
			double width = (rect.maxX - rect.minX);
			double height = rect.maxY - rect.minY;
			highlight.getHighlight(gl, false);
			gl.glBegin(PrimitiveType.LINE_LOOP.getValue());
			{
				GLUtilities.drawRoundedRectangle(gl, cx, cy, width, height, .1);
			}
			gl.glEnd();
			gl.glColor3fv(Colors.BLACK, 0);
			gl.glLineWidth(1.0f);
		}
	}

}
