package graphics;

import org.joml.Rectangled;

import com.jogamp.opengl.GL2;

import model.Card;

public interface IDrawCard {
	/**
	 * draws a card from a pile depending on the highlight and the pile's rectangle
	 * 
	 * @param gl        openGL
	 * @param card      card being drawn
	 * @param highlight highlight if its in option for current action
	 * @param rect      position of pile
	 */
	public void drawCard(GL2 gl, Card card, Highlight highlight, Rectangled rect);
}
