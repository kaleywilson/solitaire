package model;

import java.util.List;
import java.util.ListIterator;

import org.joml.Rectangled;

public class DrawPile extends Pile {

	/**
	 * constructor
	 * 
	 * @param cards
	 */
	public DrawPile(List<Card> cards) {
		super(cards, "draw", new Rectangled(1.0, 12.0, 3.0, 15.0));
	}

	@Override
	public boolean addCard(Card card) {
		return false;
	}

	/**
	 * resets the draw pile from the view pile
	 * 
	 * @param pile
	 * @return
	 */
	public boolean reset(Pile pile) {
		if (cards.isEmpty()) {
			ListIterator<Card> it = pile.rIterator();
			while (it.hasPrevious()) {
				Card card = it.previous();
				card.setVisible(false);
				card.changePile(CardPile.draw);
				cards.add(card);
				it.remove();
			}
			return true;
		}
		return false;
	}

}
