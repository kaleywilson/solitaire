package Handlers;

import java.awt.event.MouseEvent;

import model.Pile;
import solitaire.EventHandler;

public class DragMouseHandler extends IMouseHandler {
	private boolean drag;

	public DragMouseHandler(EventHandler handler) {
		super(handler);
		drag = false;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		eventHandler.setHighlight();
		eventHandler.eraseCards();
		eventHandler.drag(e);
		drag = true;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Pile pile = eventHandler.checkViewport(e);
		if (pile != null) {
			if (drag) {
				eventHandler.putBackCards();
				eventHandler.setDestinationPile(pile);
				eventHandler.moveCards();
			} else {
				eventHandler.changeMouseHandlers('k', null);
			}
		} else {
			eventHandler.putBackCards();
			eventHandler.toDefault();
		}
	}
}
