package graphics;

import com.jogamp.opengl.GL2;

import model.Pile;

public interface IPileDrawer {

	/**
	 * draws the pile being passed in on the openGL
	 * 
	 * @param gl   gl from Solitaire
	 * @param pile pile that is being drawn
	 */
	public void drawPile(GL2 gl, Pile pile);
}
