package Handlers;

import java.awt.event.KeyEvent;

import solitaire.Command;
import solitaire.EventHandler;

public class SelectCardKeyHandler extends IKeyHandler {
	private char previous;

	public SelectCardKeyHandler(EventHandler handler) {
		super(handler);
		previous = '2';
	}

	@Override
	public void keyTyped(KeyEvent e) {
		switch (e.getKeyChar()) {
		case 'a':
		case '1':
		case '0':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case 'j':
		case 'q':
		case 'k':
		case 'e':
		case '*':
			Command.doCommand(e.getKeyChar(), eventHandler, this);
			break;
		default:
			break;
		}

	}

	/** 
	 * setter for previous
	 * @param c
	 */
	public void setPrevious(char c) {
		previous = c;
	}

	/**
	 * getter for previouss
	 * @return
	 */
	public boolean checkPrevious() {
		return previous == '1';
	}
}
