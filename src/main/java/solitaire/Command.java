package solitaire;

import Handlers.DestPileAceKeyHandler;
import Handlers.DestPileKeyHandler;
import Handlers.IKeyHandler;
import Handlers.KeyHandler;
import Handlers.SelectCardKeyHandler;
import Handlers.SelectTempKeyHandler;
import Handlers.SourcePileAceKeyHandler;
import Handlers.SourcePileKeyHandler;
import model.CardDenomination;

public class Command {
	/**
	 * handles events for the script and keyhandler
	 * @param c
	 * @param eventHandler
	 * @param keyHandler
	 */
	public static void doCommand(char c, EventHandler eventHandler, IKeyHandler keyHandler) {
		switch (c) {
		case '0':
			if (((SelectCardKeyHandler) keyHandler).checkPrevious()) {
				eventHandler.setCards(CardDenomination.ten);
				eventHandler.changeKeyHandlers('d');
				((SelectCardKeyHandler) keyHandler).setPrevious('2');
			} else {
				eventHandler.changeKeyHandlers('k');
			}
			break;
		case '1':
			if (keyHandler instanceof SelectCardKeyHandler) {
				((SelectCardKeyHandler) keyHandler).setPrevious('1');
				break;
			}
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
			if (keyHandler instanceof KeyHandler) {
				eventHandler.setSourcePile('v');
				eventHandler.setCard();
				eventHandler.setDestPile(c);
				eventHandler.moveCards();
				break;
			} else if (keyHandler instanceof SourcePileKeyHandler) {
				eventHandler.setSourcePile(c);
				eventHandler.changeKeyHandlers('c');
				break;
			} else if (keyHandler instanceof SourcePileAceKeyHandler) {
				eventHandler.setSourcePile(c);
				eventHandler.setCard();
				eventHandler.changeKeyHandlers('d');
				break;
			} else if (keyHandler instanceof DestPileKeyHandler) {
				eventHandler.setDestPile(c);
				eventHandler.moveCards();
				break;
			} else if (keyHandler instanceof DestPileAceKeyHandler) {
				eventHandler.setDestPile(c);
				eventHandler.moveCards();
				break;
			} else if (keyHandler instanceof SelectCardKeyHandler) {
				eventHandler.setCards(CardDenomination.getDenom(Character.getNumericValue(c) - 1));
				eventHandler.changeKeyHandlers('d');
				break;
			} else if (keyHandler instanceof SelectTempKeyHandler) {
				eventHandler.setSourcePile(c);
				eventHandler.setCard();
				eventHandler.setDestPile('a');
				eventHandler.moveCards();
				break;
			}
			break;
		case '8':
		case '9':
			if (keyHandler instanceof SelectCardKeyHandler) {
				eventHandler.setCards(CardDenomination.getDenom(Character.getNumericValue(c) - 1));
				eventHandler.changeKeyHandlers('d');
			}
			break;
		case 'j':
		case 'J':
			if (keyHandler instanceof SelectCardKeyHandler) {
				eventHandler.setCards(CardDenomination.jack);
				eventHandler.changeKeyHandlers('d');
			}
			break;
		case 'q':
		case 'Q':
			if (keyHandler instanceof SelectCardKeyHandler) {
				eventHandler.setCards(CardDenomination.queen);
				eventHandler.changeKeyHandlers('d');
			}
			break;
		case 'k':
		case 'K':
			if (keyHandler instanceof SelectCardKeyHandler) {
				eventHandler.setCards(CardDenomination.king);
				eventHandler.changeKeyHandlers('d');
			}
			break;
		case 'A':
			if (keyHandler instanceof KeyHandler) {
				eventHandler.setSourcePile('v');
				eventHandler.setCard();
				eventHandler.setDestPile('a');
				eventHandler.moveCards();
			} else if (keyHandler instanceof SelectCardKeyHandler) {
				eventHandler.setCards(CardDenomination.ace);
				eventHandler.changeKeyHandlers('d');
			}
			break;
		case 'D':
		case 'd':
			if (keyHandler instanceof KeyHandler) {
				eventHandler.setSourcePile('d');
				eventHandler.setCard();
				eventHandler.setDestPile('v');
				eventHandler.moveCards();

			}
			break;

		case 'a':
			if (keyHandler instanceof SelectCardKeyHandler) {
				eventHandler.setCards(CardDenomination.ace);
				eventHandler.changeKeyHandlers('d');
				break;
			} else if (keyHandler instanceof KeyHandler) {
				eventHandler.changeKeyHandlers('a');
				break;
			}
		case 'm':
		case 'M':
			if (keyHandler instanceof KeyHandler) {
				eventHandler.changeKeyHandlers(c);
			}
			break;
		case 'e':
			eventHandler.changeKeyHandlers('k');
			break;
		case '*':
			eventHandler.debug();
			break;
		}
	}
}
