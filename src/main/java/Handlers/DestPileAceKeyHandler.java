package Handlers;

import java.awt.event.KeyEvent;

import solitaire.Command;
import solitaire.EventHandler;

/**
 * this class selects a destination pile among 
 * the ace piles
 * @author kaley
 *
 */
public class DestPileAceKeyHandler extends IKeyHandler {

	/**
	 * constructor
	 * @param handler
	 */
	public DestPileAceKeyHandler(EventHandler handler) {
		super(handler);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		switch (e.getKeyChar()) {
		case '1':
		case '2':
		case '3':
		case '4':
		case 'e':
		case '*':
			Command.doCommand(e.getKeyChar(), eventHandler, this);
			break;
		default:
			break;
		}

	}

}
