package solitaire;

import java.awt.event.MouseEvent;

import Handlers.IKeyHandler;
import Handlers.IMouseHandler;
import model.CardDenomination;
import model.DragPile;
import model.Pile;

public interface EventHandler {
	/**
	 * This takes a mouse event and finds the pile the mouse event was acted on.
	 * Returns null if no pile was affected by mouse event
	 * 
	 * @param e
	 * @return pile clicked on
	 */
	public Pile checkViewport(MouseEvent e);

	/**
	 * sets the source pile from which a card/s is taken from
	 * 
	 * @param c character from the keyHandler or mouseHandler
	 */
	public void setSourcePile(char c);

	/**
	 * sets the destination pile to which a card/s is trying to be placed
	 * 
	 * @param c char from the keyHandler or mouseHandler
	 */
	public void setDestPile(char c);

	/**
	 * sets the source pile from which a card/s is taken from
	 * 
	 * @param pile sets by using a Pile class
	 */
	public void setSourcePile(Pile pile);

	/**
	 * sets the destination pile to which a card/s is trying to be placed
	 * 
	 * @param pile sets by using a Pile class
	 */
	public void setDestinationPile(Pile pile);

	/**
	 * attempts to move cards from the source pile to the destination pile
	 */
	public void moveCards();

	/**
	 * sets cards to be moved (for tempPile specifically)
	 * 
	 * @param denom
	 */
	public void setCards(CardDenomination denom);

	/**
	 * sets a card to be moved using the next card from the sourcePile
	 */
	public void setCard();

	/**
	 * sets everything to default including keyhandlers, mousehandlers, dragCard,
	 * highlights
	 */
	public void toDefault();

	/**
	 * sets mousehandlers according to the character and puts in a pile that may
	 * have been affected by the switch
	 * 
	 * @param c    char by which the mouseHandler changes
	 * @param pile a pile that may have been affected, can be null
	 * @return a mousehandler
	 */
	public IMouseHandler changeMouseHandlers(char c, Pile pile);

	/**
	 * sets keyhandlers according to the character
	 * 
	 * @param c char by which the keyhandler changes
	 */
	public void changeKeyHandlers(char c);

	/**
	 * erases cards from the source pile when dragging using the mousehandler
	 */
	public void eraseCards();

	/**
	 * puts back cards to the sourcepile when releasing from a drag so the cards can
	 * be moved
	 */
	public void putBackCards();

	/**
	 * changes the cx and cy coordinates of the cards being dragged
	 * 
	 * @param e mouseEvent
	 */
	public void drag(MouseEvent e);

	/**
	 * finds the card that was clicked on if a temp pile was selected by the mouse
	 * event
	 * 
	 * @param e mouseevent
	 * @return
	 */
	public DragPile findCardTemp(MouseEvent e);

	/**
	 * returns the current Keyhandler
	 * 
	 * @return
	 */
	public IKeyHandler updateHandler();

	/**
	 * prints out the debugger
	 */
	public void debug();

	/**
	 * highlights options for where a dragged card can go among the temp piles
	 */
	public void setHighlight();

}
