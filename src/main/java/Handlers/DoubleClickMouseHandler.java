package Handlers;

import java.awt.event.MouseEvent;

import model.AcePile;
import model.Pile;
import model.TempPile;
import model.ViewPile;
import solitaire.EventHandler;

/**
 * this mousehandler handles the double click action
 * 
 * @author kaley
 *
 */
public class DoubleClickMouseHandler extends IMouseHandler {

	private Pile pile;

	public DoubleClickMouseHandler(EventHandler handler, Pile pile) {
		super(handler);
		this.pile = pile;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Pile cPile = eventHandler.checkViewport(e);
		if (cPile != null) {
			if (cPile == pile) {
				eventHandler.setDestPile('a');
				eventHandler.moveCards();
			} else {
				eventHandler.changeMouseHandlers('k', null).mouseClicked(e);
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}

}
