package Handlers;

import java.awt.event.KeyEvent;

import solitaire.Command;
import solitaire.EventHandler;

public class KeyHandler extends IKeyHandler {
	/**
	 * constructor
	 * 
	 * @param handler
	 */
	public KeyHandler(EventHandler handler) {
		super(handler);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		switch (e.getKeyChar()) {
		case 'd':
		case 'D':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case 'A':
		case 'a':
		case 'm':
		case 'M':
		case '*':
			Command.doCommand(e.getKeyChar(), eventHandler, this);
			break;
		}

	}

}
