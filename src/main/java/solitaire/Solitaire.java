package solitaire;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.swing.SwingUtilities;

import org.joml.Rectangled;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

import Handlers.DestPileAceKeyHandler;
import Handlers.DestPileKeyHandler;
import Handlers.DoubleClickMouseHandler;
import Handlers.DragMouseHandler;
import Handlers.IKeyHandler;
import Handlers.IMouseHandler;
import Handlers.KeyHandler;
import Handlers.MouseHandler;
import Handlers.SelectCardKeyHandler;
import Handlers.SelectTempKeyHandler;
import Handlers.SourcePileAceKeyHandler;
import Handlers.SourcePileKeyHandler;
import graphics.DefaultTableDrawer;
import graphics.ITableDrawer;
import graphics.TempDrawer;
import lib342.Colors;
import lib342.GraphicsWindowFP;
import lib342.ViewportLocation;
import lib342.opengl.Constants.PrimitiveType;
import lib342.opengl.Utilities;
import model.AcePile;
import model.Card;
import model.CardDenomination;
import model.CardSetUpGrid;
import model.CardSuit;
import model.Deck;
import model.DefaultDeck;
import model.DragPile;
import model.DrawPile;
import model.Pile;
import model.TempPile;
import model.TestDeck;
import mygraphicslib.GLUtilities;

public class Solitaire extends GraphicsWindowFP implements EventHandler {

	private CardSetUpGrid setup;
	private ITableDrawer tableDrawer;
	private IKeyHandler kHandler;
	private IMouseHandler mHandler;
	private IMouseHandler mMHandler;
	public static Texture texture;
	private Texture tableTexture;

	private Pile sourcePile;
	private Pile destPile;
	private DragPile card;
	private Rectangle viewport;

	private static final double left = 0.0;
	private static final double right = 22.0;
	private static final double bottom = 0.0;
	private static final double top = 16.0;
	private static final double aspectRatio = (right - left) / (top - bottom);
	private static final int initial_window_width = 800;
	private static final int initial_window_height = (int) (initial_window_width / aspectRatio);

	public Solitaire(boolean testMode, File file) {
		super(testMode ? "Solitaire -- testMode" : "Solitaire", initial_window_width, initial_window_height);
		Deck deck;
		if (testMode) {
			deck = new TestDeck();
		} else {
			deck = new DefaultDeck();
		}
		deck.shuffle();
		setup = new CardSetUpGrid(deck);
		tableDrawer = new DefaultTableDrawer();

		// key kHandler
		kHandler = new KeyHandler(Solitaire.this);
		canvas.addKeyListener(kHandler);

		// mouse handler
		mHandler = new MouseHandler(Solitaire.this);
		mMHandler = new DragMouseHandler(Solitaire.this);
		canvas.addMouseListener(mHandler);

		card = new DragPile(new ArrayList<Card>(), "drag");

		//reads file if it can
		if (file != null) {
			try {
				Reader.readFile(new FileReader(file), Solitaire.this, kHandler);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		super.init(drawable);

		try {
			InputStream is = getClass().getResourceAsStream("cardDesign.jpg");
			texture = TextureIO.newTexture(is, false, "jpg");
			is = getClass().getResourceAsStream("cardTable.jpg");
			tableTexture = TextureIO.newTexture(is, false, "jpg");

			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					canvas.requestFocus();
				}
			});
		} catch (Exception e) {
			System.err.println("Exception loading texture: " + e);
		}
		GL2 gl = (GL2) canvas.getGL();
		gl.glPointSize(10);
	}

	@Override
	public void display(GLAutoDrawable drawable) {
		GL2 gl = (GL2) canvas.getGL();

		GLUtilities.clearColorBuffer(gl, Colors.WHITE);
		GLUtilities.setWorldWindow(gl, left, right, bottom, top);

		viewport = GLUtilities.getMaximumViewport(Utilities.getCanvasWidth(canvas), Utilities.getCanvasHeight(canvas),
				aspectRatio, ViewportLocation.Center);

		gl.glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
		gl.glColor3fv(Colors.WHITE, 0);
		tableTexture.enable(gl);
		tableTexture.bind(gl);
		gl.glBegin(PrimitiveType.POLYGON.getValue());
		{
			gl.glTexCoord2d(0, 0);
			gl.glVertex2d(left, bottom);
			gl.glTexCoord2d(1, 0);
			gl.glVertex2d(right, bottom);
			gl.glTexCoord2d(1, 1);
			gl.glVertex2d(right, top);
			gl.glTexCoord2d(0, 1);
			gl.glVertex2d(left, top);

		}
		gl.glEnd();
		texture.disable(gl);

		if (!win()) {
			tableDrawer.drawTable(gl, setup);

			if (card.getDrag()) {
				drawCard(gl);
			}
		} else {
			canvas.removeMouseListener(mMHandler);
			canvas.removeMouseMotionListener(mMHandler);
			canvas.removeMouseListener(mHandler);
			canvas.removeKeyListener(kHandler);
			writeWin(gl);
		}
		gl.glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
	}

	/**
	 * creates and displays the Solitaire game
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		boolean test = false;
		File file = null;
		if (args.length > 0) {
			test = Boolean.valueOf(args[0]);
			try {
				file = new File(args[1]);
			} catch (ArrayIndexOutOfBoundsException e) {
			}
		}
		Solitaire window = new Solitaire(test, file);
		window.setVisible(true);
	}

	/**
	 * returns whether the game was won or not
	 * 
	 * @return boolean if the player has won
	 */
	private boolean win() {
		for (int i = 0; i < 7; i++) {
			Iterator<Card> it = setup.getTempPile(i).iterator();
			while (it.hasNext()) {
				Card c = it.next();
				if (!c.isVisible()) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * draws the dragged card
	 */
	private void drawCard(GL2 gl) {
		try {
			new TempDrawer().drawPile(gl, card);
		} catch (NullPointerException e) {
		}
	}

	/**
	 * writes WON! if the player has won
	 */
	private void writeWin(GL2 gl) {
		gl.glLineWidth(10.0f);
		gl.glColor3fv(Colors.WHITE, 0);
		gl.glBegin(PrimitiveType.LINE_STRIP.getValue());
		{
			gl.glVertex2d(1.0, 14.0);
			gl.glVertex2d(3.0, 1.0);
			gl.glVertex2d(4.5, 7.0);
			gl.glVertex2d(6.0, 1.0);
			gl.glVertex2d(8.0, 14.0);
		}
		gl.glEnd();
		gl.glBegin(PrimitiveType.LINE_LOOP.getValue());
		{
			gl.glVertex2d(9.0, 14.0);
			gl.glVertex2d(9.0, 1.0);
			gl.glVertex2d(12.0, 1.0);
			gl.glVertex2d(12.0, 14.0);
		}
		gl.glEnd();
		gl.glBegin(PrimitiveType.LINE_STRIP.getValue());
		{
			gl.glVertex2d(13.0, 1.0);
			gl.glVertex2d(13.0, 14.0);
			gl.glVertex2d(16.0, 1.0);
			gl.glVertex2d(16.0, 14.0);
		}
		gl.glEnd();
		gl.glBegin(PrimitiveType.LINES.getValue());
		{
			gl.glVertex2d(18.0, 14.0);
			gl.glVertex2d(18.0, 5.0);
			gl.glVertex2d(18.0, 3.0);
			gl.glVertex2d(18.0, 3.25);
		}
		gl.glEnd();

	}

	/**
	 * all implements methods from the EventHandler
	 * 
	 * ------------------------------------------------------------------------
	 */
	@Override
	public void drag(MouseEvent e) {
		try {
			card.setDrag(true);
			card.setCoord(getX(e.getX()), getY(canvas.getHeight() - e.getY()));
			canvas.repaint();
		} catch (NullPointerException m) {

		}
	}

	@Override
	public void toDefault() {
		card = new DragPile(new ArrayList<Card>());
		removeHighlight();
		changeKeyHandlers('k');
		changeMouseHandlers('k', null);
		canvas.removeMouseMotionListener(mMHandler);
		canvas.removeMouseListener(mMHandler);
		canvas.repaint();

	}

	@Override
	public void setHighlight() {
		for (int i = 0; i < 7; i++) {
			if (((TempPile) setup.getTempPile(i)).canAdd(card.getFirst())) {
				setup.getTempPile(i).setHighlight(true);
			}
		}
	}

	@Override
	public void setSourcePile(Pile pile) {
		sourcePile = pile;
		if (sourcePile instanceof DrawPile) {
			if (sourcePile.isEmpty()) {
				setup.resetDrawPile();
			}
		}
	}

	@Override
	public void setDestinationPile(Pile pile) {
		destPile = pile;
	}

	@Override
	public void setCard() {
		card = new DragPile(new ArrayList<Card>());
		card.addCard(sourcePile.getNext());
	}

	@Override
	public void setCards(CardDenomination denom) {
		try {
			List<Card> c = sourcePile.getCards(sourcePile.getCardByDenom(denom));
			for (int k = 0; k < c.size(); k++) {
				card.addCard(c.get(k));
			}
		} catch (IndexOutOfBoundsException e) {

		}
	}

	@Override
	public void moveCards() {
		if (destPile instanceof AcePile && sourcePile instanceof TempPile && card.size() > 1) {
		} else {
			try {
				setup.moveCard(card.getFirst(), destPile, sourcePile);
			} catch (IndexOutOfBoundsException e) {
			} catch (NullPointerException e) {
			}
		}

		toDefault();
	}

	@Override
	public void eraseCards() {
		Iterator<Card> it = card.iterator();
		while (it.hasNext()) {
			Card c = it.next();
			try {
				if (sourcePile instanceof TempPile) {
					((TempPile) sourcePile).eraseCard(c);
				} else {
					sourcePile.removeCard(c);
				}
			} catch (IndexOutOfBoundsException e) {
			}
		}
	}

	@Override
	public void putBackCards() {
		Iterator<Card> it = card.iterator();
		while (it.hasNext()) {
			Card c = it.next();
			sourcePile.reAddCard(c);
		}
	}

	@Override
	public IKeyHandler updateHandler() {
		return kHandler;
	}

	@Override
	public Pile checkViewport(MouseEvent e) {
		double x = getX(e.getX());
		double y = getY(canvas.getHeight() - e.getY());
		Pile pile = null;
		if (y > bottom && y < top && x < right && x > left) {

			if ((pile = checkPile(x, y, setup.getDrawPile())) == null) {
				if ((pile = checkPile(x, y, setup.getViewPile())) == null) {
					for (int i = 0; i <= CardSuit.values().length - 1; i++) {
						if ((pile = checkPile(x, y, setup.getAcePile(i))) != null) {
							return pile;
						}
					}
					if (pile == null) {
						for (int i = 0; i < 7; i++) {
							if ((pile = checkPile(x, y, setup.getTempPile(i))) != null) {
								return pile;
							}
						}
					}
				}
			}
		}
		return pile;
	}

	@Override
	public DragPile findCardTemp(MouseEvent e) {
		double gap = 0.3 * 3;
		double x = getX(e.getX());
		double y = getY(canvas.getHeight() - e.getY());

		if (9 < sourcePile.size() * gap + 3) {
			gap = 6 / (double) sourcePile.size();
		}

		Rectangled rect = sourcePile.getRectangle();
		ListIterator<Card> it = sourcePile.rIterator();
		int i = sourcePile.size() - 1;
		while (it.hasPrevious()) {
			Card c = it.previous();
			if (c.isVisible()) {
				if (x > rect.minX && x < rect.maxX) {
					if (y > rect.minY - gap * i && y < rect.maxY - gap * i) {
						card = new DragPile(sourcePile.getCards(c));
						return card;
					}
				}
			}
			i--;
		}
		return null;
	}

	@Override
	public void setSourcePile(char c) {
		switch (c) {
		case 'd':
			sourcePile = setup.getDrawPile();
			if (sourcePile.isEmpty()) {
				setup.resetDrawPile();
			}
			break;
		case 'v':
			sourcePile = setup.getViewPile();
			break;
		case '1':
			if (kHandler instanceof SourcePileAceKeyHandler) {
				sourcePile = setup.getAcePile(0);
			} else {
				sourcePile = setup.getTempPile(0);
			}
			break;
		case '2':
			if (kHandler instanceof SourcePileAceKeyHandler) {
				sourcePile = setup.getAcePile(1);
			} else {
				sourcePile = setup.getTempPile(1);
			}
			break;
		case '3':
			if (kHandler instanceof SourcePileAceKeyHandler) {
				sourcePile = setup.getAcePile(2);
			} else {
				sourcePile = setup.getTempPile(2);
			}
			break;
		case '4':
			if (kHandler instanceof SourcePileAceKeyHandler) {
				sourcePile = setup.getAcePile(3);
			} else {
				sourcePile = setup.getTempPile(3);
			}
			break;
		case '5':
			sourcePile = setup.getTempPile(4);
			break;
		case '6':
			sourcePile = setup.getTempPile(5);
			break;
		case '7':
			sourcePile = setup.getTempPile(6);
			break;
		}
	}

	@Override
	public void setDestPile(char c) {
		switch (c) {
		case 'a':
			destPile = setup.getAcePile(card.getFirst().getSuit());
			break;
		case 'v':
			destPile = setup.getViewPile();
			break;
		case '1':
			if (kHandler instanceof DestPileAceKeyHandler) {
				destPile = setup.getAcePile(0);
			} else {
				destPile = setup.getTempPile(0);
			}
			break;
		case '2':
			if (kHandler instanceof DestPileAceKeyHandler) {
				destPile = setup.getAcePile(1);
			} else {
				destPile = setup.getTempPile(1);
			}
			break;
		case '3':
			if (kHandler instanceof DestPileAceKeyHandler) {
				destPile = setup.getAcePile(2);
			} else {
				destPile = setup.getTempPile(2);
			}
			break;
		case '4':
			if (kHandler instanceof DestPileAceKeyHandler) {
				destPile = setup.getAcePile(3);
			} else {
				destPile = setup.getTempPile(3);
			}
			break;
		case '5':
			destPile = setup.getTempPile(4);
			break;
		case '6':
			destPile = setup.getTempPile(5);
			break;
		case '7':
			destPile = setup.getTempPile(6);
			break;

		}
	}

	@Override
	public void changeKeyHandlers(char c) {
		switch (c) {

		case 'a':
			setKeyHandler(new SelectTempKeyHandler(Solitaire.this));
			break;
		case 'c':
			setKeyHandler(new SelectCardKeyHandler(Solitaire.this));
			break;
		case 'd':
			setKeyHandler(new DestPileKeyHandler(Solitaire.this));
			break;
		case 'k':
			setKeyHandler(new KeyHandler(Solitaire.this));
			break;
		case 'm':
			setKeyHandler(new SourcePileKeyHandler(Solitaire.this));
			break;
		case 'M':
			setKeyHandler(new SourcePileAceKeyHandler(Solitaire.this));
			break;
		default:
			break;
		}

	}

	@Override
	public IMouseHandler changeMouseHandlers(char c, Pile pile) {
		switch (c) {
		case 'A':
			return setMouseHandler(new DoubleClickMouseHandler(Solitaire.this, pile));
		case 'k':
			return setMouseHandler(new MouseHandler(Solitaire.this));
		case 'd':
			return setDragMouseHandler(new DragMouseHandler(Solitaire.this));

		}
		return null;

	}

	@Override
	public void debug() {
		System.out.println("DRAW" + setup.getDrawPile().toString());
		System.out.println("VIEW" + setup.getViewPile().toString());
		System.out.println("ACE CLUBS");
		if (setup.checkAcePiles(CardSuit.Club)) {
			System.out.println(setup.getAcePile(CardSuit.Club).toString());
		}
		System.out.println("ACE DIAMONDS");
		if (setup.checkAcePiles(CardSuit.Diamond)) {
			System.out.println(setup.getAcePile(CardSuit.Diamond).toString());
		}
		System.out.println("ACE HEARTS");
		if (setup.checkAcePiles(CardSuit.Heart)) {
			System.out.println(setup.getAcePile(CardSuit.Heart).toString());
		}
		System.out.println("ACE SPADES");
		if (setup.checkAcePiles(CardSuit.Spade)) {
			System.out.println(setup.getAcePile(CardSuit.Spade).toString());
		}

		for (int i = 0; i < 7; i++) {
			System.out.println("TEMP " + (i + 1) + setup.getTempPile(i).toString());
		}

	}

	/**
	 * methods below here are used as helpers for the implemented methods
	 * 
	 * ------------------------------------------------------------------------
	 */

	/**
	 * this method checks a pile passed in reference to x and y taken from the
	 * checkViewport
	 * 
	 * @param x    x coordinate of mouse
	 * @param y    y coordinate of mouse
	 * @param pile pile that needs to be check
	 * @return the pile clicked on, null if the pile was not clicked
	 */
	private Pile checkPile(double x, double y, Pile pile) {
		Rectangled rect = pile.getRectangle();
		if (pile instanceof TempPile) {
			if (x > rect.minX && x < rect.maxX) {
				if (y > rect.maxY - 9 && y < rect.maxY) {
					return pile;
				}
			}
		} else if (x > rect.minX && x < rect.maxX) {
			if (y > rect.minY && y < rect.maxY) {
				return pile;
			}
		}
		return null;
	}

	/**
	 * removes highlight from temp piles
	 */
	private void removeHighlight() {
		for (int i = 0; i < 7; i++) {
			setup.getTempPile(i).setHighlight(false);
		}
	}

	/**
	 * transforms the y parameter into world coordinates
	 * 
	 * @param y canvas coordinate y
	 * @return the world coordinate y
	 */
	private double getY(double y) {
		double yMin;
		double yMax;
		if (canvas.getHeight() > canvas.getWidth()) {
			double h = canvas.getWidth() / aspectRatio;
			yMin = canvas.getHeight() / 2 + canvas.getY() - h / 2;
			yMax = canvas.getHeight() / 2 + canvas.getY() + h / 2;
		} else {
			yMin = canvas.getY();
			yMax = canvas.getY() + canvas.getHeight();
		}
		return (y - yMin) * (top - bottom) / (yMax - yMin) + bottom;
	}

	/**
	 * transforms the x parameter into world coordinates
	 * 
	 * @param x canvas coordinate x
	 * @return world coordinate x
	 */
	private double getX(double x) {
		double xMin;
		double xMax;
		if (canvas.getHeight() < canvas.getWidth()) {
			double w = canvas.getHeight() * aspectRatio;
			xMin = canvas.getWidth() / 2 + canvas.getX() - w / 2;
			xMax = canvas.getWidth() / 2 + canvas.getX() + w / 2;
		} else {
			xMin = canvas.getX();
			xMax = canvas.getX() + canvas.getHeight();
		}
		return (x - xMin) * (right - left) / (xMax - xMin) + left;
	}

	/**
	 * removes and adds new keyhandler
	 * 
	 * @param h new keyhandler
	 */
	private void setKeyHandler(IKeyHandler h) {
		canvas.removeKeyListener(kHandler);
		kHandler = h;
		canvas.addKeyListener(kHandler);
	}

	/**
	 * removes and adds new mousehandler to the canvas mouselistener
	 * 
	 * @param h new mouseHandler
	 * @return
	 */
	private IMouseHandler setMouseHandler(IMouseHandler h) {
		canvas.removeMouseListener(mHandler);
		canvas.removeMouseMotionListener(mHandler);
		canvas.removeMouseListener(mMHandler);
		canvas.removeMouseMotionListener(mMHandler);
		mHandler = h;
		canvas.addMouseListener(mHandler);
		return mHandler;
	}

	/**
	 * removes and adds new mousehandler to the canvas mouselistner and mouse motion
	 * listener
	 * 
	 * @param h
	 * @return
	 */
	private IMouseHandler setDragMouseHandler(IMouseHandler h) {
		canvas.removeMouseListener(mHandler);
		canvas.removeMouseMotionListener(mHandler);
		canvas.removeMouseListener(mMHandler);
		canvas.removeMouseMotionListener(mMHandler);
		mMHandler = h;
		canvas.addMouseListener(mMHandler);
		canvas.addMouseMotionListener(mMHandler);
		return mMHandler;
	}

}
