package model;

import java.util.Collections;

public class DefaultDeck extends Deck {

	public DefaultDeck() {
		super();
	}

	@Override
	public void shuffle() {
		Collections.shuffle(cards);
	}

}
