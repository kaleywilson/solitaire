package model;

import java.util.ArrayList;

public class CardSetUpGrid {
	protected Pile[][] pileGrid;

	/**
	 * constructor
	 * 
	 * @param deck
	 */
	public CardSetUpGrid(Deck deck) {
		createSetUp(deck);
	}

	/**
	 * sets up the card table and assigns piles to the 2d array
	 * 
	 * @param deck
	 */
	protected void createSetUp(Deck deck) {
		pileGrid = new Pile[2][7];
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 7; j++) {
				pileGrid[i][j] = null;
			}
		}
		for (int i = 0; i < 7; i++) {
			pileGrid[1][i] = new TempPile(new ArrayList<Card>(), i);
		}
		// deals the temp piles
		for (int j = 7; j > 0; j--) { // indicates how many piles to give too
			for (int i = 7 - j; i < 7; i++) { // indicates where to start
				Card card = deck.nextCard();
				pileGrid[1][i].addCard(card);
			}
		}

		pileGrid[0][0] = new DrawPile(deck.getCards());
		pileGrid[0][1] = new ViewPile(new ArrayList<Card>());
		for (int i = 0; i < 4; i++) {
			pileGrid[0][i + 3] = new AcePile(i);
		}
		revealTemps();
	}

	/**
	 * reveal first card of temp piles
	 */
	private void revealTemps() {
		for (int j = 0; j < 7; j++) {
			pileGrid[1][j].getNext().setVisible(true);
		}
	}

	/**
	 * moves card from the source pile to the destination pile
	 * 
	 * @param card     card moving
	 * @param goPile   destination pile
	 * @param fromPile source pile
	 * @return
	 */
	public boolean moveCard(Card card, Pile goPile, Pile fromPile) {
		return fromPile.moveCard(goPile, card);
	}

	/**
	 * resets the drawPile
	 * 
	 * @return
	 */
	public boolean resetDrawPile() {
		return ((DrawPile) pileGrid[0][0]).reset(pileGrid[0][1]);
	}

	/**
	 * returns the temp pile according to the index
	 * 
	 * @param i index
	 * @return TempPile
	 */
	public Pile getTempPile(int i) {
		return pileGrid[1][i];
	}

	/**
	 * returns the draw pile
	 * 
	 * @return Pile
	 */
	public Pile getDrawPile() {
		return pileGrid[0][0];
	}

	/**
	 * returns the view pile
	 * 
	 * @return
	 */
	public Pile getViewPile() {
		return pileGrid[0][1];
	}

	/**
	 * returns an ace pile associated with the suit it returns an empty ace pile if
	 * there is not ace pile with that suit
	 * 
	 * @param suit
	 * @return Pile
	 */
	public Pile getAcePile(CardSuit suit) {
		for (int i = 3; i < 7; i++) {
			try {
				if (((AcePile) pileGrid[0][i]).getSuit() == suit) {
					return pileGrid[0][i];
				}
			} catch (NullPointerException e) {
			}
		}
		for (int i = 3; i < 7; i++) {

			if (((AcePile) pileGrid[0][i]).getSuit() == null) {
				((AcePile) pileGrid[0][i]).setSuit(suit);
				return pileGrid[0][i];
			}
		}
		return null;
	}

	/**
	 * returns an ace pile according to the index
	 * 
	 * @param i index
	 * @return Pile
	 */
	public Pile getAcePile(int i) {
		return pileGrid[0][i + 3];
	}

	/**
	 * returns a boolean if there's an ace pile for a specific suit
	 * 
	 * @param suit CardSuit
	 * @return boolean
	 */
	public boolean checkAcePiles(CardSuit suit) {
		for (int i = 3; i < 7; i++) {
			try {
				if (((AcePile) pileGrid[0][i]).getSuit() == suit) {
					return true;
				}
			} catch (NullPointerException e) {
			}
		}
		return false;
	}

}
