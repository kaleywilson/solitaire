package graphics;

import org.joml.Rectangled;

import com.jogamp.opengl.GL2;

import lib342.Colors;
import lib342.opengl.Constants.PrimitiveType;
import model.Card;
import model.CardColor;
import model.CardDenomination;
import model.CardSuit;
import mygraphicslib.GLUtilities;

public class VisibleCard implements IDrawCard {

	@Override
	public void drawCard(GL2 gl, Card card, Highlight highlight, Rectangled rect) {
		double cx = (rect.maxX - rect.minX) / 2 + rect.minX;
		double cy = (rect.maxY - rect.minY) / 2 + rect.minY;
		double width = (rect.maxX - rect.minX);
		double height = rect.maxY - rect.minY;

		// draw filled in rectangle
		gl.glColor3fv(Colors.WHITE, 0);
		gl.glBegin(PrimitiveType.POLYGON.getValue());
		{
			GLUtilities.drawRoundedRectangle(gl, cx, cy, width, height, .05);
		}
		gl.glEnd();

		// draw highlgiht
		highlight.getHighlight(gl, true);
		gl.glBegin(PrimitiveType.LINE_LOOP.getValue());
		{
			GLUtilities.drawRoundedRectangle(gl, cx, cy, width, height, .05);
		}
		gl.glEnd();
		gl.glLineWidth(1.0f);

		// draw the suit and deonomination
		CardColor.getColor(gl, card.getSuit());
		CardSuit.drawSuit(gl, card.getSuit(), rect);
		CardDenomination.drawDenomination(gl, card.getDenomination(), rect);

		gl.glColor3fv(Colors.BLACK, 0);
	}

}
