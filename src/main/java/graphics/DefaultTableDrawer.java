package graphics;

import com.jogamp.opengl.GL2;

import model.CardSetUpGrid;
import model.CardSuit;

public class DefaultTableDrawer implements ITableDrawer {

	/**
	 * constructor
	 */
	public DefaultTableDrawer() {
	}

	@Override
	public void drawTable(GL2 gl, CardSetUpGrid setup) {
		IPileDrawer pileDrawer = new DefaultPileDrawer();

		pileDrawer.drawPile(gl, setup.getDrawPile());

		pileDrawer.drawPile(gl, setup.getViewPile());

		for (int i = 0; i <= CardSuit.values().length - 1; i++) {
			pileDrawer.drawPile(gl, setup.getAcePile(i));
		}

		pileDrawer = new TempDrawer();
		for (int i = 0; i < 7; i++) {
			pileDrawer.drawPile(gl, setup.getTempPile(i));

		}
	}

}
