package Handlers;

import java.awt.event.KeyEvent;

import solitaire.Command;
import solitaire.EventHandler;

/**
 * this class selects a destination pile among the temp piles
 * 
 * @author kaley
 *
 */
public class DestPileKeyHandler extends IKeyHandler {

	public DestPileKeyHandler(EventHandler handler) {
		super(handler);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		switch (e.getKeyChar()) {
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case 'e':
		case '*':
			Command.doCommand(e.getKeyChar(), eventHandler, this);
			break;
		default:
			break;
		}
	}

}
