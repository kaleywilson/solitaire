package Handlers;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import solitaire.EventHandler;

public abstract class IKeyHandler extends KeyAdapter {
	protected EventHandler eventHandler;

	public IKeyHandler(EventHandler handler) {
		eventHandler = handler;
	}

	@Override
	public abstract void keyTyped(KeyEvent e);
}
