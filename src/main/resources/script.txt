#  Draw, should be JH
d 

#  Move AD to Ace pile, reveals 3D on TP6 
a6 

#  Move QH to pile 1, reveals KH on TP7
m7Q1 

#  Draw, should be 10H, 9H, ..., 5H 
6D 

#  Move 5H to bottom of pile 2 
2

#  Draw 4H, ..., AH and place AH into Ace pile 
4d A

#  Draw KS, QS, then move QS to TP7 
d d 7

#  Draw JS and place onto TP1 
d 1

#  Draw 10S, ..., 7S and place 7S onto TP4 
4d 4 

#  Draw 6S, 5S, 4S, place 4S onto TP2 
3d 2

#  Move 2D from TP6 to TP2, reveals 6D on TP6, move to TP4, reveals 10D on TP6
m632 m664

#  Move 10D from TP6 to TP1, reveals 2C on TP6, move 2C to TP2, revealing 8C
m6101 m622

#  Draw 3S and put onto TP5 
d 5

#  Draw 2S, AS, put AS and 2S onto Ace Pile, move 5S from top of view pile to TP4 
2d 2A 4 

#  Move 4D and 3S from TP5 to TP4, reveals 7D on TP5 
m544

#  Move 6C through 2C from TP2 to TP5, reveals QC on TP2 
m265

#  Draw JH (refreshes draw pile), place on TP7 
d 7

#  Move 3S from TP4 to ace pile 
a 4

#  Draw 10H, ..., 7H.  Place 7H on TP6 
4d 6

#  Move 6C through 2C from TP5 to TP6 
m566 

#  Move QC from TP2 to TP3, then move KD and QC from TP3 to TP2, reveals 5C on TP3 
m2Q3 m3K2

#  Move 3S from AP3 down to TP4 
M34