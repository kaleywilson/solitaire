package graphics;

import com.jogamp.opengl.GL2;

import lib342.Colors;

public class Highlight {
	private boolean highlight;

	/**
	 * constructor
	 * 
	 * @param h if highlight is on or off
	 */
	public Highlight(boolean h) {
		highlight = h;
	}

	/**
	 * sets the openGL depending on the highlight and if card is visible
	 * 
	 * @param gl      gl from Solitaire window
	 * @param visible if card is visible
	 */
	public void getHighlight(GL2 gl, boolean visible) {
		if (highlight) {
			gl.glColor3fv(Colors.GREEN, 0);
			gl.glLineWidth(5.0f);
		} else {
			if (visible) {
				gl.glColor3fv(Colors.BLACK, 0);
			} else {
				gl.glColor3fv(Colors.WHITE, 0);
			}

		}
	}
}
