package model;

import java.util.List;

import org.joml.Rectangled;

public class DragPile extends Pile {
	private double cx; // x-center of pile
	private double cy; // y-center of pile
	private boolean drag; // tracks whether its being dragged

	/**
	 * constructor
	 * 
	 * @param cards list of cards it has
	 * @param name  CardPile name
	 */
	public DragPile(List<Card> cards, String name) {
		super(cards, name, new Rectangled());
		drag = false;
	}

	/**
	 * another constructor
	 * 
	 * @param cards
	 */
	public DragPile(List<Card> cards) {
		super(cards, "drag", new Rectangled());
		drag = false;
	}

	@Override
	public boolean addCard(Card card) {
		return cards.add(card);
	}

	@Override
	public Rectangled getRectangle() {
		return new Rectangled(cx - 1.0, cy - 1.5, cx + 1.0, cy + 1.5);
	}

	/**
	 * sets coordinates to be dragged
	 * 
	 * @param x
	 * @param y
	 */
	public void setCoord(double x, double y) {
		cx = x;
		cy = y;
	}

	/**
	 * setter for drag boolean
	 * 
	 * @param d
	 */
	public void setDrag(boolean d) {
		drag = d;
	}

	/**
	 * getter for drag boolean
	 * 
	 * @return boolean
	 */
	public boolean getDrag() {
		return drag;
	}
}
