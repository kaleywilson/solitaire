package graphics;

import com.jogamp.opengl.GL2;

import model.CardSetUpGrid;

public interface ITableDrawer {

	/**
	 * draws the CardSetUpGrid on the openGL
	 * 
	 * @param gl    openGL from Solitaire
	 * @param setup card setup from the Solitaire
	 */
	public void drawTable(GL2 gl, CardSetUpGrid setup);
}
