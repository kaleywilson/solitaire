package Handlers;

import java.awt.event.MouseEvent;

import model.AcePile;
import model.DrawPile;
import model.Pile;
import model.TempPile;
import model.ViewPile;
import solitaire.EventHandler;

public class MouseHandler extends IMouseHandler {

	/**
	 * constructor
	 * 
	 * @param handler
	 */
	public MouseHandler(EventHandler handler) {
		super(handler);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Pile pile = eventHandler.checkViewport(e);
		if (pile instanceof DrawPile) {
			eventHandler.setSourcePile(pile);
			eventHandler.setCard();
			eventHandler.setDestPile('v');
			eventHandler.moveCards();
		} else if (pile instanceof ViewPile || pile instanceof TempPile) {
			eventHandler.setSourcePile(pile);
			eventHandler.setCard();
			eventHandler.changeMouseHandlers('A', pile);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Pile pile = eventHandler.checkViewport(e);
		if (pile instanceof ViewPile || pile instanceof AcePile) {
			eventHandler.setSourcePile(pile);
			eventHandler.setCard();
			eventHandler.changeMouseHandlers('d', null);
		} else if (pile instanceof TempPile) {
			eventHandler.setSourcePile(pile);
			if (eventHandler.findCardTemp(e) != null) {
				eventHandler.changeMouseHandlers('d', null);
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
	}
}
